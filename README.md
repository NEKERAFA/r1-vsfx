# Reto 1: Mi propia DEMO, DEMOSCENE

Repositorio de Rafael Alcalde Azpiazu para el Reto 1 de la asignatura de Efectos visuales y sonoros del Master Universitario en Diseño y Programación de Videojuegos de la UOC.

## ⚙️ Compilar el proyecto

No debería haber problema en las dependencias porque dentro de los archivos se incluye una comprobación para saber cómo incluir las cabeceras de SDL, que cambian entre Windows y Unix:

```cpp
#ifdef _WIN32
#include <SDL.h>
#include <SDL_mixer.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#endif
```

Para compilar el proyecto se necesitan incluir los siguientes ficheros:

```
main.cpp pec1.cpp program.cpp utils.cpp effects/plasma.cpp effects/fire.cpp effects/wave.cpp transitions/fade.cpp transitions/circle.cpp transitions/move.cpp
```

Una vez construido, se necesitan los archivos `caramelldansen.ogg`, `splash.png` y `background.png` para poder ejecutarlo.

## 🗃️ Estructura de archivos

El repositorio está dividido en carpetas por semanas, que contienen cada una los retos propuestos para cada semana. Se ha usado CMake para la compilación de cada reto, para lo cual se necesita descargar el submodulo de cmake/sdl2.

```sh
git submodule update --init --recursive
```

En la carpeta `effects` están los archivos fuente de la implementación de cada efecto. Estos heredan de la clase `SDL_Effects`, definida en la cabecera `program.h`. Cada definición está en la cabecera `effects.h`.

De forma similar, las transiciones están definidas en la carpeta `transitions`, y cada objeto está definido en la cabecera `transitions.h`, y todos derivar de la `clase SDL_Transition`, definida en la cabecera `program.h`.

El resto de ficheros sirven de apoyo para la ejecución del programa. En `config.h` están las definiciones de constantes que usa el programa, como el tiempo entre efectos o las dimensiones de la ventana. En `utils.cpp` tenemos funciones de apoyo usadas en varios efectos o transiciones. En `main.cpp` tenemos la implementación del `int main()`, además del inicio de los sistemas SDL y de la aplicación, y en `pec1.cpp` tenemos la definición del programa en sí, con la inicialización de paletas, precalculo de efectos, etc.

## Reto

### 🌌 Efectos implementados

- **Plasma**

  Efecto de plasma entre distintas paletas, implementado en `plasma.cpp`. Hay dos buffers que se suman para obtener los valles y crestas de las funciones sinusoidales. La paleta de colores va cambiando cada vez que sale.

  ![Efecto de plasma](extra/plasma.png)

- **Fuego**

  Efecto de fuego en donde se usa una matriz de convolución, implementado en `fire.cpp`. Se sigue lo expuesto en el cuaderno.

  ![Efecto de fuego](extra/fire.png)

- **Distorsión de olas**

  Se implementa en wave.cpp, y distorsiona una imagen siguiendo funciones sinusoidales. Hay dos buffers que se suman para obtener los desplazamientos tanto en vertical como horizontal de cada píxel de la imagen.

  ![Efecto de olas](extra/wave.png)

### 🌠 Transiciones implementadas

- **Fade In**

  Transición de negro a la imagen del efecto. Implementado en `fade.cpp`. Se usa para iniciar los efectos.

- **Fade Out**

  Transición de la imagen del efecto a negro. Implementado en `fade.cpp`. Se usa para iniciar al cerrar el programa.

- **Círculo**

  Transición entre un efecto y otro que aparece dentro de un círculo hasta que rellena la pantalla. Implementado en `circle.cpp`.

- **Desplazamiento**

  Pinta los dos efectos, haciendo que uno desplace a otro de forma lineal. Se escoge aleatoriamente cada uno de los desplazamientos (arriba, abajo, izquierda, derecha). Está implementado en `move.cpp`.
