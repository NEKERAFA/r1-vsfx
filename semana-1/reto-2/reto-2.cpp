#include <iostream>
#include <array>
#include <random>
#include <functional>
#include <cstdint>
#include <cmath>

// Para unix (Linux/macOS) la ruta al header es SDL2/SDL.h, cosa que en Windows es solo el fichero
#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "config.h"
#include "star.h"

const int TICKS_PER_FRAME = 1000 / FRAMES_PER_SECOND;

std::default_random_engine generator;
std::uniform_int_distribution<int> position_dist(0, SCREEN_WIDTH * SCREEN_HEIGHT - 1);
std::uniform_real_distribution<float> real_distribution(0.0f, 1.0f);

auto position_random = std::bind (position_dist, generator);
auto real_random = std::bind (real_distribution, generator);

std::array<star*, STARS_COUNT> stars;

star* init_star(star* object = NULL) {
    int position = position_random();
    int pos_x = position % SCREEN_WIDTH;
    int pos_y = position / SCREEN_WIDTH;

    float dir_x = object == NULL ? (float)pos_x - (float)SCREEN_WIDTH / 2.0f : real_random() * 2.0f - 1.0f;
    float dir_y = object == NULL ? (float)pos_y - (float)SCREEN_HEIGHT / 2.0f : real_random() * 2.0f - 1.0f;
    float dir_len = std::sqrt(dir_x * dir_x + dir_y * dir_y);
    dir_x = dir_x / dir_len;
    dir_y = dir_y / dir_len;
    float velocity = real_random() * 16.0 + 4.0;

    if (object == NULL) {
        return new star(pos_x, pos_y, velocity * dir_x, velocity * dir_y);
    }

    object->restart(pos_x, pos_y, velocity * dir_x, velocity * dir_y);
    return object;
}

void init_stars() {
    for (int i = 0; i < STARS_COUNT; i++) {
        stars[i] = init_star();
    }
}

// Función para actualizar estrellas
void update(uint64_t delta_time) {
    for (int i = 0; i < STARS_COUNT; i++) {
        stars[i]->update(delta_time);

        // Comprobamos si la estrella se ha ido de la pantalla
        vector2f position = stars[i]->get_position();
        if ((position.x <= 0) || (position.x >= SCREEN_WIDTH) || (position.y <= 0) || (position.y >= SCREEN_HEIGHT)) {
            // Reiniciamos la estrella
            init_star(stars[i]);
        }
    }
}

// Función para dibujar las estrellas
void draw(SDL_Surface*& surface) {
    SDL_LockSurface(surface);

    for (int i = 0; i < STARS_COUNT; i++) {
        vector2f position = stars[i]->get_position();
        int pos_x = std::floor(position.x + 0.5);
        int pos_y = std::floor(position.y + 0.5);
        if ((pos_x >= 0) && (pos_x < SCREEN_WIDTH) && (pos_y >= 0) && (pos_y < SCREEN_HEIGHT)) { // Tenía puesto las restricciones mayores con <=
            Uint32* pixel = (Uint32*) ((Uint8*) surface->pixels + pos_y * surface->pitch + pos_x * surface->format->BytesPerPixel);
            *pixel = SDL_MapRGB(surface->format, 0xFF, 0xFF, 0xFF);
        }
    }

    SDL_UnlockSurface(surface);
}

bool init_window(SDL_Window*& window, SDL_Surface*& surface) {
    // Se inicializa el módulo SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "SDL could not initalize! SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }
    
    // Se crea la ventana
    window = SDL_CreateWindow("Reto 2", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        std::cerr << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }

    // Obtiene la superficie de la ventana
    surface = SDL_GetWindowSurface(window);
    return true;
}

bool check_quit(SDL_Event& event) {
    // Comprobamos si tenemos que finalizar el programa
    if (event.type == SDL_QUIT) {
        return true;
    }

    // Comprobamos si se ha presionado la tecla ESCAPE
    if (event.type == SDL_KEYDOWN) {
        if (event.key.keysym.sym == SDLK_ESCAPE) {
            return true;
        }
    }

    return false;
}

int main(int argc, char* args[]) {
    // Ventana donde se renderizará el contenido
    SDL_Window* window = NULL;
    // Superficie principal
    SDL_Surface* surface = NULL;

    // Se inicializa el módulo SDL
    if (init_window(window, surface)) {
        init_stars();

        bool quit = false;
        uint64_t last_time = 0;
        uint64_t current_time = 0;
        uint64_t delta_time = 0;
        SDL_Event event;

        while (!quit) {
            SDL_PollEvent(&event);

            // Si no tenemos que finalizar, actualizamos las estrellas
            if (!(quit = check_quit(event))) {
                current_time = SDL_GetTicks64();
                delta_time = current_time - last_time;

                // Actualizamos las estrellas
                update(delta_time);

                // Preparamos la superficie
                SDL_FillRect(surface, NULL, SDL_MapRGB(surface->format, 0, 0, 0));

                // Dibujamos las estrellas
                draw(surface);

                // Actualizamos el buffer
                SDL_UpdateWindowSurface(window);

                // Esperamos el tiempo entre frames
                if (delta_time < TICKS_PER_FRAME) {
                    SDL_Delay(TICKS_PER_FRAME - delta_time);
                }
                last_time = current_time;
            }
        }
    }

    // Se destruye la ventana
    SDL_DestroyWindow(window);

    // Finalizamos los subsistemas de SDL
    SDL_Quit();

    return 0;
}