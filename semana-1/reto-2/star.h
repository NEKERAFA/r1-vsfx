#include <cstdint>

#include "config.h"

#ifndef STAR_H
#define STAR_H

/// @brief Representa un vector(x, y) flotante
struct vector2f {
    float x = 0.0f;
    float y = 0.0f;
};

/// @brief Representa una estrella
class star {
    private:
        vector2f position;
        vector2f velocity;

    public:
        /// @brief Inicializa la estrella
        /// @param x posición en el eje x en pixeles
        /// @param y posición en el eje y en pixeles
        /// @param vx velocidad en el eje x en pixeles / segundos
        /// @param vy velocidad en el eje y en pixeles / segundos
        star(int x, int y, float vx, float vy): position({(float)x, (float)y}), velocity({(float)vx, (float)vy}) {}

        /// @brief Reinicia el estado de la estrella
        /// @param x posición en el eje x en pixeles
        /// @param y posición en el eje y en pixeles
        /// @param vx velocidad en el eje x en pixeles / segundos
        /// @param vy velocidad en el eje y en pixeles / segundos
        void restart(int x, int y, float vx, float vy) {
            position.x = (float)x;
            position.y = (float)y;
            velocity.x = vx;
            velocity.y = vy;
        }

        void update(uint64_t delta_time) {
            position.x += (float)delta_time / 1000.0f * velocity.x;
            position.y += (float)delta_time / 1000.0f * velocity.y;
        }

        vector2f& get_position() {
            return position;
        }
};

#endif // STAR_H