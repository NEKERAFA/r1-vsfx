#ifndef CONFIG_H
#define CONFIG_H

// Tamaño de la ventana
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

// Frames por segundo
const int FRAMES_PER_SECOND = 60;

// Número de estrellas
const int STARS_COUNT = 1000;

#endif // CONFIG_H