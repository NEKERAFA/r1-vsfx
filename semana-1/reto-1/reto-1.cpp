#include <iostream>

// Para unix (Linux/macOS) la ruta al header es SDL2/SDL.h, cosa que en Windows es solo el fichero
#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

// Tamaño de la ventana
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

int main(int argc, char* args[]) {
    // Ventana donde se renderizará el contenido
    SDL_Window* window = NULL;

    // Superficie principal
    SDL_Surface* surface = NULL;

    // Se inicializa el módulo SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "SDL could not initalize! SDL_Error: " << SDL_GetError() << std::endl;
    } else {
        // Se crea la ventana
        window = SDL_CreateWindow("Reto 1", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
        if (window == NULL) {
            std::cerr << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        } else {
            // Obtiene la superficie de la ventana
            surface = SDL_GetWindowSurface(window);

            // Llenamos la superficie de blanco
            SDL_FillRect(surface, NULL, SDL_MapRGB(surface->format, 0xFF, 0xFF, 0xFF));

            // Actualizamos la superficie
            SDL_UpdateWindowSurface(window);

            // Comprobamos si tenemos que salir de la app
            SDL_Event event;
            while (event.type != SDL_QUIT) SDL_PollEvent(&event);
        }
    }

    // Se destruye la ventana
    SDL_DestroyWindow( window );

    // Finalizamos los subsistemas de SDL
    SDL_Quit();

    return 0;
}