#include <iostream>
#include <cmath>
#include <cstdint>

// Para unix (Linux/macOS) la ruta al header es SDL2/SDL.h, cosa que en Windows es solo el fichero
#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "config.h"

const float PI = 3.1415926535;

const int BUFFER_WIDTH = SCREEN_WIDTH * 2;
const int BUFFER_HEIGHT = SCREEN_HEIGHT * 2;
const int TICKS_PER_FRAME = 1000 / FRAMES_PER_SECOND;

const SDL_Color COLOR_1 = {0x9D, 0xEE, 0x90};
const SDL_Color COLOR_2 = {0xF0, 0xF0, 0xF0};
const SDL_Color COLOR_3 = {0xF4, 0x7f, 0x60};

Uint8* buffer1 = NULL;
Uint8* buffer2 = NULL;
int buffer1_xoffset = 0, buffer1_yoffset = 0, buffer2_xoffset = 0, buffer2_yoffset = 0;

float clamp01(float v) { return v > 0 ? (v < 1 ? v : 1) : 0; }
Uint8 lerp(Uint8 a, Uint8 b, float t) { return a + clamp01(t) * (b - a); }

void init_buffers() {
    buffer1 = new Uint8[BUFFER_WIDTH * BUFFER_HEIGHT];
    buffer2 = new Uint8[BUFFER_WIDTH * BUFFER_HEIGHT];

    int pos = 0;
    for (int y = 0; y < BUFFER_HEIGHT; y++) {
        for (int x = 0; x < BUFFER_WIDTH; x++) {
            buffer1[pos] = (Uint8)(64.0 + 63.0 * std::sin(std::hypot(SCREEN_HEIGHT - y, SCREEN_WIDTH - x) / 16.0));
            buffer2[pos] = (Uint8)(64.0 + 63.0 * std::sin(x / (37.0 + 15.0 * std::cos(y / 74.0))) * std::cos(y / (31.0 + 11.0 * std::sin(x / 57.0))));
            pos++;
        }
    }
}

bool init_window(SDL_Window*& window, SDL_Surface*& surface) {
    // Se inicializa el módulo SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "SDL could not initalize! SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }
    
    // Se crea la ventana
    window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        std::cerr << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }

    // Obtiene la superficie de la ventana
    surface = SDL_GetWindowSurface(window);
    return true;
}

bool check_events(SDL_Event& event) {
    // Comprobamos si tenemos que finalizar el programa
    if (event.type == SDL_QUIT) {
        return true;
    }

    // Comprobamos si se ha presionado la tecla ESCAPE
    if (event.type == SDL_KEYDOWN) {
        if (event.key.keysym.sym == SDLK_ESCAPE) {
            return true;
        }
    }

    return false;
}

void update(Uint64 current_time) {
    // se actualiza cada ventana siguiendo funciones sinusoidales
    // value = Amplitud * sin(2 * pi * t / frecuencia de oscilación (en ms))
    buffer1_xoffset = (SCREEN_WIDTH - 1) * (std::cos(2.0 * PI * current_time / 8640.0) + 1.0) / 2.0;
    buffer1_yoffset = (SCREEN_HEIGHT - 1) * (std::sin(2.0 * PI * current_time / 9460.0) + 1.0) / 2.0;
    buffer2_xoffset = (SCREEN_WIDTH - 1) * (std::sin(2.0 * PI * current_time / 4530.0) + 1.0) / 2.0;
    buffer2_yoffset = (SCREEN_HEIGHT - 1) * (std::cos(2.0 * PI * current_time / 2590.0) + 1.0) / 2.0;
}

// Establece el valor del pixel
void draw_pixel(SDL_Surface*& surface, int x, int y, Uint8 r, Uint8 g, Uint8 b) {
    if ((x >= 0) && (x < SCREEN_WIDTH) && (y >= 0) && (y < SCREEN_HEIGHT)) {
        Uint32* pixel = (Uint32*) ((Uint8*) surface->pixels + y * surface->pitch + x * surface->format->BytesPerPixel);
        *pixel = SDL_MapRGB(surface->format, r, g, b);
    }
}

void draw(SDL_Surface*& surface) {
    SDL_LockSurface(surface);

    for (int y = 0; y < SCREEN_HEIGHT; y++) {
        for (int x = 0; x < SCREEN_WIDTH; x++) {
            // Obtenemos los valores de los dos bufferes
            Uint8 value1 = buffer1[x + buffer1_xoffset + (y + buffer1_yoffset) * BUFFER_WIDTH];
            Uint8 value2 = buffer2[x + buffer2_xoffset + (y + buffer2_yoffset) * BUFFER_WIDTH];
            Uint8 value = value1 + value2 % UINT8_MAX;

            // Se calcula el color mediante interpolación
            Uint8 r, g, b;
            if (value < 128) {
                r = lerp(COLOR_1.r, COLOR_2.r, (float)value / 128.0f);
                g = lerp(COLOR_1.g, COLOR_2.g, (float)value / 128.0f);
                b = lerp(COLOR_1.b, COLOR_2.b, (float)value / 128.0f);
            } else {
                r = lerp(COLOR_2.r, COLOR_3.r, ((float)value - 128.0f) / 128.0f);
                g = lerp(COLOR_2.g, COLOR_3.g, ((float)value - 128.0f) / 128.0f);
                b = lerp(COLOR_2.b, COLOR_3.b, ((float)value - 128.0f) / 128.0f);
            }

            // Pintamos el pixel
            draw_pixel(surface, x, y, r, g, b);
        }
    }

    SDL_UnlockSurface(surface);
}

int main(int argc, char* args[]) {
    // Ventana donde se renderizará el contenido
    SDL_Window* window = NULL;
    // Superficie principal
    SDL_Surface* surface = NULL;

    if (init_window(window, surface)) {
        // Inicializamos los buffers
        init_buffers();

        // Inicializamos el bucle principal
        bool quit = false;
        Uint64 last_time = 0ul, current_time = 0ul, delta_time = 0ul;
        SDL_Event event;

        while (!quit) {
            // Comprobamos los eventos
            SDL_PollEvent(&event);

            // Si no tenemos que finalizar, actualizamos el efecto
            if (!(quit = check_events(event))) {
                // Calculamos el tiempo transcurrido desde el último frame
                current_time = SDL_GetTicks64();
                delta_time = current_time - last_time;

                // Actualizamos el efecto
                update(current_time);

                // Dibujamos el efecto en pantalla
                draw(surface);

                // Actualizamos el buffer
                SDL_UpdateWindowSurface(window);

                // Esperamos el tiempo entre frames
                if (delta_time < TICKS_PER_FRAME) {
                    SDL_Delay(TICKS_PER_FRAME - delta_time);
                }
                last_time = current_time;
            }
        }
        
        delete[] buffer1;
        delete[] buffer2;
    }

    // Se destruye la ventana
    SDL_DestroyWindow(window);

    // Finalizamos los subsistemas de SDL
    SDL_Quit();

    return 0;
}