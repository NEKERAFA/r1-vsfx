#include <iostream>
#include <random>
#include <functional>
#include <cmath>
#include <cstdint>
#include <cstring>

// Para unix (Linux/macOS) la ruta al header es SDL2/SDL.h, cosa que en Windows es solo el fichero
#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "config.h"

const float PI = 3.1415926535;

const int BUFFER_WIDTH = SCREEN_WIDTH * 2;
const int BUFFER_HEIGHT = SCREEN_HEIGHT * 2;
const int TICKS_PER_FRAME = 1000 / FRAMES_PER_SECOND;

// Buffers de desplazamiento vertical y horizontal
int8_t* movement_v = NULL;
int8_t* movement_h = NULL;

// Offset de la ventana de los buffers de desplazamiento
int movement_v_offset = 0, movement_h_offset = 0;

void init_buffers() {
    // Inicializamos los buffers
    movement_v = new int8_t[BUFFER_HEIGHT];
    movement_h = new int8_t[BUFFER_WIDTH];

    // Establecemos el valor de los buffers
    int pos;
    for (pos = 0; pos < BUFFER_WIDTH; pos++) {
        movement_h[pos] = static_cast<uint8_t>(WAVE_LENGTH * std::sin(2.0 * PI * pos / (WAVE_LENGTH * 2)));
        if (pos < BUFFER_HEIGHT)
            movement_v[pos] = static_cast<uint8_t>(WAVE_LENGTH * std::sin(2.0 * PI * pos / (WAVE_LENGTH * 2)));
    }
}

void free_buffers() {
    // Liberamos los buffers
    delete[] movement_h;
    delete[] movement_v;
}

bool init_image(SDL_Surface*& image) {
    image = SDL_LoadBMP("image.bmp");
    if (image == NULL) {
        std::cerr << "Unable to load image.bmp! SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }

    std::cout << "pitch: " << image->pitch << ", bpp: " << (int)image->format->BytesPerPixel << std::endl;

    return true;
}

bool init_window(SDL_Window*& window, SDL_Surface*& surface) {
    // Se inicializa el módulo SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "SDL could not initalize! SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }
    
    // Se crea la ventana
    window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        std::cerr << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }

    // Obtiene la superficie de la ventana
    surface = SDL_GetWindowSurface(window);
    std::cout << "pitch: " << surface->pitch << ", bpp: " << (int)surface->format->BytesPerPixel << std::endl;
    return true;
}

bool check_events(SDL_Event& event) {
    // Comprobamos si tenemos que finalizar el programa
    if (event.type == SDL_QUIT) {
        return true;
    }

    // Comprobamos si se ha presionado la tecla ESCAPE
    if (event.type == SDL_KEYDOWN) {
        if (event.key.keysym.sym == SDLK_ESCAPE) {
            return true;
        }
    }

    return false;
}

void update(Uint64 current_time) {
    // se actualiza cada ventana siguiendo funciones sinusoidales
    movement_v_offset = (SCREEN_HEIGHT - 1) * ((std::sin(2.0 * PI * current_time / (BUFFER_HEIGHT * 8)) + 1.0) / 2.0);
    movement_h_offset = (SCREEN_WIDTH - 1) * ((std::cos(2.0 * PI * current_time / (BUFFER_WIDTH * 16)) + 1.0) / 2.0);
}

void draw(SDL_Surface*& surface, SDL_Surface*& image) {
    Uint8* image_buffer = (Uint8*) image->pixels;
    int image_bpp = image->format->BytesPerPixel;
    int image_pitch = image->pitch;
    
    Uint8* surface_buffer = (Uint8*) surface->pixels;
    int surface_bpp = surface->format->BytesPerPixel;
    int surface_pitch = surface->pitch;

    Uint8* dst;

    SDL_LockSurface(surface);

    for (int y = 0; y < SCREEN_HEIGHT; y++) {
        dst = ((Uint8*) surface_buffer + y * surface_pitch);
        for (int x = 0; x < SCREEN_WIDTH; x++) {
            // Obtenemos los valores desplazamiento
            int dx = x + (movement_v[y + movement_v_offset] >> 2);
            int dy = y + (movement_h[x + movement_h_offset] >> 2);

            // Obtenemos el valor del pixel
            if ((dx >= 0) && (dx < SCREEN_WIDTH - 1) && (dy >= 0) && (dy < SCREEN_HEIGHT - 1)) {
                Uint32* src = (Uint32*) ((Uint8*) image_buffer + dy * image_pitch + dx * image_bpp);
                *((Uint32*) dst) = *src;
            } else {
                *((Uint32*) dst) = 0;
            }

            dst += image_bpp;
        }
    }

    SDL_UnlockSurface(surface);
}

int main(int argc, char* args[]) {
    // Ventana donde se renderizará el contenido
    SDL_Window* window = NULL;
    // Superficie principal
    SDL_Surface* surface = NULL;
    // Imagen
    SDL_Surface* image = NULL;

    // Inicializamos la imagen
    if (init_window(window, surface)) {
        // Cargamos la imagen
        if (init_image(image)) {
            // Inicializamos los buffers
            init_buffers();

            // Inicializamos el bucle principal
            bool quit = false;
            Uint64 last_time = 0ul, current_time = 0ul, delta_time = 0ul;
            SDL_Event event;

            while (!quit) {
                // Comprobamos los eventos
                SDL_PollEvent(&event);

                // Si no tenemos que finalizar, actualizamos el efecto
                if (!(quit = check_events(event))) {
                    // Calculamos el tiempo transcurrido desde el último frame
                    current_time = SDL_GetTicks64();
                    delta_time = current_time - last_time;

                    // Actualizamos el efecto
                    update(current_time);

                    // Dibujamos el efecto en pantalla
                    draw(surface, image);

                    // Actualizamos el buffer
                    SDL_UpdateWindowSurface(window);

                    // Esperamos el tiempo entre frames
                    if (delta_time < TICKS_PER_FRAME) {
                        SDL_Delay(TICKS_PER_FRAME - delta_time);
                    }
                    last_time = current_time;
                }
            }
            
            SDL_FreeSurface(image);
            image = NULL;

            if (movement_v != NULL) {
                delete[] movement_v;
                movement_v = NULL;
            }

            if (movement_h != NULL) {
                delete[] movement_h;
                movement_h = NULL;
            }
        }
    }

    // Se destruye la ventana
    SDL_DestroyWindow(window);
    window = NULL;

    // Finalizamos los subsistemas de SDL
    SDL_Quit();

    return 0;
}