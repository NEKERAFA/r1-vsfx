cmake_minimum_required(VERSION 3.0)
project(Reto5 VERSION 1.0.0)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/sdl2)

find_package(SDL2 REQUIRED)

add_executable(${PROJECT_NAME} reto-5.cpp)
target_link_libraries(${PROJECT_NAME} SDL2::Main)