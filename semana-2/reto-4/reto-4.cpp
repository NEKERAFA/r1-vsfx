#include <iostream>
#include <random>
#include <functional>
#include <cmath>
#include <cstdint>
#include <cstring>

// Para unix (Linux/macOS) la ruta al header es SDL2/SDL.h, cosa que en Windows es solo el fichero
#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "config.h"

const int TICKS_PER_FRAME = 1000 / FRAMES_PER_SECOND;

const int FIRE_WIDTH = 5;
const int FIRE_HEIGHT = 5;
const int FIRE_FILTER[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0};

SDL_Color* colors = NULL;

Uint8* last_buffer = NULL;
Uint8* current_buffer = NULL;

inline float clamp01(float v) { return v > 0 ? (v < 1 ? v : 1) : 0; }
Uint8 lerp(Uint8 a, Uint8 b, float t) { return a + clamp01(t) * (b - a); }

std::default_random_engine generator;
std::uniform_real_distribution<float> real_distribution(0.0f, 1.0f);
auto real_random = std::bind (real_distribution, generator);

void add_fire(Uint8*& buffer) {
    for (int x = 0; x < real_random() * 8; x++) {
        buffer[SCREEN_WIDTH * (SCREEN_HEIGHT - 1) + (int)(SCREEN_WIDTH * real_random())] = UINT8_MAX * real_random();
    }
}

void init_buffers() {
    last_buffer = new Uint8[SCREEN_WIDTH * SCREEN_HEIGHT];
    current_buffer = new Uint8[SCREEN_WIDTH * SCREEN_HEIGHT];

    add_fire(last_buffer);
}

void add_color(Uint8 min, Uint8 max, SDL_Color min_color, SDL_Color max_color) {
    Uint8 length = max - min;
    for (int i = min; i <= max; i++) {
        colors[i].r = lerp(min_color.r, max_color.r, (float)(i - min) / (float)length);
        colors[i].g = lerp(min_color.g, max_color.g, (float)(i - min) / (float)length);
        colors[i].b = lerp(min_color.b, max_color.b, (float)(i - min) / (float)length);
    }
}

void init_colors() {
    colors = new SDL_Color[256];

    add_color(0x00, 0x17, {0x00, 0x00, 0x00}, {0x20, 0x00, 0x40});
    add_color(0x18, 0x2F, {0x20, 0x00, 0x40}, {0xFF, 0x00, 0x00});
    add_color(0x30, 0x3F, {0xFF, 0x00, 0x00}, {0xFF, 0xFF, 0x00});
    add_color(0x40, 0x7F, {0xFF, 0xFF, 0x00}, {0xFF, 0xFF, 0x00});
    add_color(0x80, 0xFF, {0xFF, 0xFF, 0xFF}, {0xFF, 0xFF, 0xFF});
}

bool init_window(SDL_Window*& window, SDL_Surface*& surface) {
    // Se inicializa el módulo SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "SDL could not initalize! SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }
    
    // Se crea la ventana
    window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        std::cerr << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }

    // Obtiene la superficie de la ventana
    surface = SDL_GetWindowSurface(window);
    return true;
}

bool check_events(SDL_Event& event) {
    // Comprobamos si tenemos que finalizar el programa
    if (event.type == SDL_QUIT) {
        return true;
    }

    // Comprobamos si se ha presionado la tecla ESCAPE
    if (event.type == SDL_KEYDOWN) {
        if (event.key.keysym.sym == SDLK_ESCAPE) {
            return true;
        }
    }

    return false;
}

// Aplica el filtro sobre un pixel
Uint8 convolve(int x, int y) {
    int acc = 0;
    int sum = 0;

    for (int j = 0; j < FIRE_HEIGHT; j++) {
        for (int i = 0; i < FIRE_WIDTH; i++) {
            if (FIRE_FILTER[i + j * FIRE_WIDTH] == 0) continue;
            int nx = i - FIRE_WIDTH / 2 + x;
            if (nx < 0 || nx >= SCREEN_WIDTH) continue;
            int ny = j - FIRE_HEIGHT / 2 + y;
            if (ny < 0 || ny >= SCREEN_HEIGHT) continue;

            acc += last_buffer[nx + ny * SCREEN_WIDTH] * FIRE_FILTER[i + j * FIRE_WIDTH];
            sum++;
        }
    }

    return (Uint8)(acc / sum);
}

void update(Uint64 current_time) {
    // Actualizamos el nuevo buffer
    for (int y = 0; y < SCREEN_HEIGHT; y++) {
        for (int x = 0; x < SCREEN_WIDTH; x++) {
            current_buffer[x + y * SCREEN_WIDTH] = convolve(x, y);
        }
    }

    // Añadimos nuevo fuego
    add_fire(current_buffer);
}

// Establece el valor del pixel
void draw_pixel(SDL_Surface*& surface, int x, int y, Uint8 r, Uint8 g, Uint8 b) {
    if ((x >= 0) && (x < SCREEN_WIDTH) && (y >= 0) && (y < SCREEN_HEIGHT)) {
        Uint32* pixel = (Uint32*) ((Uint8*) surface->pixels + y * surface->pitch + x * surface->format->BytesPerPixel);
        *pixel = SDL_MapRGB(surface->format, r, g, b);
    }
}

void draw(SDL_Surface*& surface) {
    SDL_LockSurface(surface);

    for (int y = 0; y < SCREEN_HEIGHT; y++) {
        for (int x = 0; x < SCREEN_WIDTH; x++) {
            // Obtenemos los valores de los dos bufferes
            Uint8 value = last_buffer[x + y * SCREEN_WIDTH];
            SDL_Color color = colors[value];

            // Pintamos el pixel
            draw_pixel(surface, x, y, color.r, color.g, color.b);
        }
    }

    SDL_UnlockSurface(surface);
}

int main(int argc, char* args[]) {
    // Ventana donde se renderizará el contenido
    SDL_Window* window = NULL;
    // Superficie principal
    SDL_Surface* surface = NULL;

    if (init_window(window, surface)) {
        // Inicializamos los buffers
        init_buffers();
        // Inicializamos el buffer de color
        init_colors();

        // Inicializamos el bucle principal
        bool quit = false;
        Uint64 last_time = 0ul, current_time = 0ul, delta_time = 0ul;
        SDL_Event event;

        while (!quit) {
            // Comprobamos los eventos
            SDL_PollEvent(&event);

            // Si no tenemos que finalizar, actualizamos el efecto
            if (!(quit = check_events(event))) {
                // Calculamos el tiempo transcurrido desde el último frame
                current_time = SDL_GetTicks64();
                delta_time = current_time - last_time;

                // Actualizamos el efecto
                update(current_time);

                // Dibujamos el efecto en pantalla
                draw(surface);

                // Actualizamos el buffer
                SDL_UpdateWindowSurface(window);

                // Establecemos el frame anterior
                std::memcpy(last_buffer, current_buffer, sizeof(Uint8) * SCREEN_WIDTH * SCREEN_HEIGHT);

                // Esperamos el tiempo entre frames
                if (delta_time < TICKS_PER_FRAME) {
                    SDL_Delay(TICKS_PER_FRAME - delta_time);
                }
                last_time = current_time;
            }
        }
        
        if (colors != NULL) delete[] colors;
        if (last_buffer != NULL) delete[] last_buffer;
        if (current_buffer != NULL) delete[] current_buffer;
    }

    // Se destruye la ventana
    SDL_DestroyWindow(window);

    // Finalizamos los subsistemas de SDL
    SDL_Quit();

    return 0;
}