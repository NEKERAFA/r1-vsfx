#ifndef CONFIG_H
#define CONFIG_H

// Nombre de la ventana
const char* WINDOW_TITLE = "Reto 4";

// Tamaño de la ventana
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

// Frames por segundo
const int FRAMES_PER_SECOND = 60;

#endif // CONFIG_H