#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#ifndef PEC1_UTILS_H
#define PEC1_UTILS_H

// Definición de PI
const float PI = 3.1415926535;

// Representa un vector(x, y) con valores enteros
struct vector2i {
    int x = 0;
    int y = 0;
};

// Limita un valor entre 0 y 1
float clamp01(float v);

// Interpola linearmente entre dos valores
Uint8 lerp(Uint8 a, Uint8 b, float t);

// Añade un gradiente de colores a un array
void add_gradient(SDL_Color* colors, Uint8 min, Uint8 max, SDL_Color min_color, SDL_Color max_color);

// Establece el valor del pixel
void draw_pixel(SDL_Surface* surface, int x, int y, Uint8 r, Uint8 g, Uint8 b);

// Obtiene el pixel
Uint32* get_pixel(SDL_Surface* surface, int x, int y);

#endif // PEC1_UTILS_H
