#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "config.h"
#include "program.h"

SDL_Effect::SDL_Effect() { 
    layer = SDL_CreateRGBSurface(0, SCREEN_WIDTH, SCREEN_HEIGHT, 32, 0, 0, 0, 0);
    SDL_SetSurfaceBlendMode(layer, SDL_BLENDMODE_BLEND);
}

SDL_Effect::~SDL_Effect() { SDL_FreeSurface(layer); };

SDL_Surface* SDL_Effect::get_layer() { return layer; }

bool SDL_Transition::is_completed() { return this->completed; }
