#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "../program.h"
#include "../transitions.h"

void FadeIn::update(Uint64 delta_time) {
    if (remain_time < delta_time) {
        completed = true;
        return;
    }

    remain_time -= delta_time;
}

void FadeIn::draw(SDL_Surface* surface, SDL_Surface* _last_effect, SDL_Surface* current_effect) {
    SDL_SetSurfaceAlphaMod(current_effect, (1.0f - ((float)remain_time / (float)TRANS_MAX_TIME)) * 255.0f);
    SDL_BlitSurface(current_effect, NULL, surface, NULL);
}

void FadeOut::update(Uint64 delta_time) {
    if (remain_time < delta_time) {
        completed = true;
        return;
    }

    remain_time -= delta_time;
}

void FadeOut::draw(SDL_Surface* surface, SDL_Surface* _last_effect, SDL_Surface* current_effect) {
    SDL_SetSurfaceAlphaMod(current_effect, ((float)remain_time / (float)TRANS_MAX_TIME) * 255.0f);
    SDL_BlitSurface(current_effect, NULL, surface, NULL);
}
