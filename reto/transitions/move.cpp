#include <random>
#include <functional>

#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "../program.h"
#include "../transitions.h"

extern std::random_device generator;
extern std::uniform_real_distribution<float> real_distribution;

Move::Move() { start(); }

void Move::start() {
    movement = static_cast<Movement>(real_distribution(generator) * 4);
    remain_time = TRANS_MAX_TIME;
    completed = false;
}

void Move::update(Uint64 delta_time) {
    if (remain_time < delta_time) {
        completed = true;
        return;
    }

    remain_time -= delta_time;
}

void Move::draw(SDL_Surface* surface, SDL_Surface* last_effect, SDL_Surface* current_effect) {
    SDL_Rect last_rect = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};
    SDL_Rect current_rect = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};

    switch (movement) {
        case TOP:
            last_rect.y = (1.0f - (float)remain_time / (float)TRANS_MAX_TIME) * SCREEN_HEIGHT;
            current_rect.y = -((float)remain_time / (float)TRANS_MAX_TIME * SCREEN_HEIGHT);
            break;
        case LEFT:
            last_rect.x = (1.0f - (float)remain_time / (float)TRANS_MAX_TIME) * SCREEN_WIDTH;
            current_rect.x = -((float)remain_time / (float)TRANS_MAX_TIME * SCREEN_WIDTH);
            break;
        case BOTTOM:
            last_rect.y = (-1.0f + (float)remain_time / (float)TRANS_MAX_TIME) * SCREEN_HEIGHT;
            current_rect.y = ((float)remain_time / (float)TRANS_MAX_TIME) * SCREEN_HEIGHT;
            break;
        case RIGHT:
            last_rect.x = (-1.0f + (float)remain_time / (float)TRANS_MAX_TIME) * SCREEN_WIDTH;
            current_rect.x = ((float)remain_time / (float)TRANS_MAX_TIME) * SCREEN_WIDTH;
            break;
    }

    SDL_BlitSurface(last_effect, NULL, surface, &last_rect);
    SDL_BlitSurface(current_effect, NULL, surface, &current_rect);
}
