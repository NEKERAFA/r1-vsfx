#include <cmath>

#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "../config.h"
#include "../program.h"
#include "../utils.h"
#include "../transitions.h"

const float RADIUS_PER_TIME = 64.0f / 500.0f; // 16 px per 500 ms

Circle::Circle() { start(); }

void Circle::start() {
    radius = 0.0f;
    completed = false;
}

void Circle::update(Uint64 delta_time) {
    if (!completed) {
        radius += RADIUS_PER_TIME * delta_time;
    }

    // Se comprueba que el círculo ocupe la pantalla
    if ((2 * radius) > std::sqrt(std::pow(SCREEN_WIDTH, 2) + std::pow(SCREEN_HEIGHT, 2))) {
        completed = true;
    }
}

void Circle::draw(SDL_Surface* surface, SDL_Surface* last_effect, SDL_Surface* current_effect) {
    SDL_LockSurface(surface);

    Uint8* dst = (Uint8*) surface->pixels;
    for (int y = 0; y < SCREEN_HEIGHT; y++) {
        for (int x = 0; x < SCREEN_WIDTH; x++) {
            // Se comprueba el color a dibujar
            Uint32* src;
            if ((std::pow(x - (SCREEN_WIDTH / 2), 2) + std::pow(y - (SCREEN_HEIGHT / 2), 2)) <= (radius * radius)) {
                src = get_pixel(current_effect, x, y);
            } else {
                src = get_pixel(last_effect, x, y);
            }

            // Se establece el valor del pixel
            *((Uint32*) dst) = *src;
            dst += surface->format->BytesPerPixel;
        }
    }

    SDL_UnlockSurface(surface);
}
