#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "config.h"
#include "program.h"

#ifndef PEC1_TRANSITIONS_H
#define PEC1_TRANSITIONS_H

class FadeIn : public SDL_Transition {
    private:
        Uint64 remain_time = TRANS_MAX_TIME;

    public:
        FadeIn() {}
        ~FadeIn() {}
        void update(Uint64 delta_time) override;
        void draw(SDL_Surface* surface, SDL_Surface* _last_effect, SDL_Surface* current_effect);
};

class FadeOut : public SDL_Transition {
    private:
        Uint64 remain_time = TRANS_MAX_TIME;

    public:
        FadeOut() {}
        ~FadeOut() {}
        void update(Uint64 delta_time) override;
        void draw(SDL_Surface* surface, SDL_Surface* _last_effect, SDL_Surface* current_effect);
};

class Circle : public SDL_Transition {
    private:
        float radius;

    public:
        Circle();
        ~Circle() {}
        void start();
        void update(Uint64 delta_time) override;
        void draw(SDL_Surface* surface, SDL_Surface* last_effect, SDL_Surface* current_effect);
};

class Move : public SDL_Transition {
    private:
        enum Movement { TOP, LEFT, BOTTOM, RIGHT };
        Movement movement;
        Uint64 remain_time;

    public:
        Move();
        ~Move() {}
        void start();
        void update(Uint64 delta_time) override;
        void draw(SDL_Surface* surface, SDL_Surface* last_effect, SDL_Surface* current_effect);
};

#endif // PEC1_TRANSITIONS_H
