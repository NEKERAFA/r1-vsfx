#include <iostream>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_image.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#endif

#include "config.h"
#include "pec1.h"

Splash::Splash(SDL_Surface* surface) {
    image = IMG_Load(SPLASH_FILE);
    if (image == NULL) {
        std::cout << "Warning: Unable to load image! SDL_Image Error: " << IMG_GetError() << std::endl;
    }

    // Se convierte la imagen en el formato de la superficie
    SDL_Surface* optimizedSurface = SDL_ConvertSurface(image, surface->format, 0);
    if (optimizedSurface == NULL) {
        std::cout << "Warning: Unable to optimize image! SDL_Error: " << SDL_GetError() << std::endl;
    } else {
        // Eliminamos la imagen cargada y establecemos la optimizada
        SDL_FreeSurface(image);
        image = optimizedSurface;
    }

    SDL_SetSurfaceBlendMode(image, SDL_BLENDMODE_BLEND);
}

Splash::~Splash() { if (image != NULL) SDL_FreeSurface(image); }

void Splash::require_completed() {
    status = FADE_OUT;
    remain_time = TRANS_MAX_TIME;
}

bool Splash::is_completed() { return status == COMPLETED; }

void Splash::update(Uint64 delta_time) {
    if (status != COMPLETED) {
        if (remain_time < delta_time) {
            switch (status) {
                case FADE_IN:
                    status = SHOW;
                    remain_time += TRANS_MAX_TIME * 2;
                    break;
                case SHOW:
                    status = FADE_OUT;
                    remain_time += TRANS_MAX_TIME;
                    break;
                case FADE_OUT:
                    status = COMPLETED;
                    break;
            }
        }

        if (status != COMPLETED) {
            remain_time -= delta_time;
        }
    }
}

void Splash::draw(SDL_Surface* surface) {
    if (status == FADE_IN || status == FADE_OUT) {
        Uint8 alpha = ((float)remain_time / (float)TRANS_MAX_TIME) * 255.0f;
        if (status == FADE_IN) alpha = 255 - alpha;
        SDL_SetSurfaceAlphaMod(image, alpha);
    }

    SDL_BlitSurface(image, NULL, surface, NULL);
}
