#include <iostream>
#include <random>
#include <functional>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_image.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#endif

#include "program.h"
#include "effects.h"
#include "transitions.h"
#include "pec1.h"

// Paletas de colores
SDL_Color* palette_plasma1 = NULL;
SDL_Color* palette_plasma2 = NULL;
SDL_Color* palette_plasma3 = NULL;
SDL_Color* palette_fire = NULL;

// Inicializamos las paletas
void init_palettes() {
    palette_plasma1 = new SDL_Color[256];
    add_gradient(palette_plasma1, 0, 127, {0x9D, 0xEE, 0x90}, {0xF0, 0xF0, 0xF0});
    add_gradient(palette_plasma1, 128, 255, {0xF0, 0xF0, 0xF0}, {0xF4, 0x7f, 0x60});

    palette_plasma2 = new SDL_Color[256];
    add_gradient(palette_plasma2, 0, 127, {0xF0, 0xB0, 0x60}, {0xF0, 0xF0, 0xF0});
    add_gradient(palette_plasma2, 128, 255, {0xF0, 0xF0, 0xF0}, {0x50, 0xD0, 0xF0});

    palette_plasma3 = new SDL_Color[256];
    add_gradient(palette_plasma3, 0x00, 0x3F, {0xF0, 0x60, 0x60}, {0xF0, 0xE4, 0xA0});
    add_gradient(palette_plasma3, 0x40, 0x7F, {0xF0, 0xE4, 0xA0}, {0x80, 0xF0, 0x80});
    add_gradient(palette_plasma3, 0x80, 0xBF, {0x80, 0xF0, 0x80}, {0xA0, 0xE0, 0xF4});
    add_gradient(palette_plasma3, 0xC0, 0xFF, {0xA0, 0xE0, 0xF4}, {0xD0, 0xA0, 0xF0});

    palette_fire = new SDL_Color[256];
    add_gradient(palette_fire, 0x00, 0x17, {0x00, 0x00, 0x00}, {0x20, 0x00, 0x40});
    add_gradient(palette_fire, 0x18, 0x2F, {0x20, 0x00, 0x40}, {0xFF, 0x00, 0x00});
    add_gradient(palette_fire, 0x30, 0x3F, {0xFF, 0x00, 0x00}, {0xFF, 0xFF, 0x00});
    add_gradient(palette_fire, 0x40, 0x7F, {0xFF, 0xFF, 0x00}, {0xFF, 0xFF, 0x00});
    add_gradient(palette_fire, 0x80, 0xFF, {0xFF, 0xFF, 0xFF}, {0xFF, 0xFF, 0xFF});
}

// Destruimos las paletas
void free_palettes() {
    delete[] palette_plasma1;
    delete[] palette_plasma2;
    delete[] palette_plasma3;
    delete[] palette_fire;
}

// Efectos
PlasmaEffect* plasma_effect = NULL;
FireEffect* fire_effect = NULL;
WaveEffect* wave_effect = NULL;

// Inicializamos los efectos
void init_effects() {
    plasma_effect = new PlasmaEffect();
    plasma_effect->set_palette(palette_plasma1);

    fire_effect = new FireEffect();
    fire_effect->set_palette(palette_fire);

    wave_effect = new WaveEffect();
}

// Destruimos los efectos
void free_effects() {
    delete plasma_effect;
}

SDL_Surface* image = NULL;

// Iniciamos la imagen
bool init_image(SDL_Surface* surface) {
    image = IMG_Load(IMAGE_FILE);
    if (image == NULL) {
        std::cout << "Warning: Unable to load image! SDL_Image Error: " << IMG_GetError() << std::endl;
        return false;
    }

    // Se convierte la imagen en el formato de la superficie
    SDL_Surface* optimizedSurface = SDL_ConvertSurface(image, surface->format, 0);
    if (optimizedSurface == NULL) {
        std::cout << "Warning: Unable to optimize image! SDL_Error: " << SDL_GetError() << std::endl;
    } else {
        // Eliminamos la imagen cargada y establecemos la optimizada
        SDL_FreeSurface(image);
        image = optimizedSurface;
    }

    return true;
}

// Transiciones
FadeIn* fade_in = NULL;
FadeOut* fade_out = NULL;
Circle* circle_trans = NULL;
Move* move_trans = NULL;

// Inicializamos las transiciones
void init_transitions() {
    fade_in = new FadeIn();
    fade_out = new FadeOut();
    circle_trans = new Circle();
    move_trans = new Move();
}

// Destruimos las transiciones
void free_transitions() {
    delete fade_in;
    delete fade_out;
    delete circle_trans;
    delete move_trans;
}

// Inicializa el programa
PEC1::PEC1(SDL_Surface* surface) {
    splash = new Splash(surface);

    init_palettes();
    init_effects();
    init_transitions();

    if (init_image(surface)) {
        wave_effect->set_image(image);
    }
}

// Finaliza el programa
PEC1::~PEC1() {
    free_palettes();
    free_effects();
    free_transitions();

    SDL_FreeSurface(image);
}

// Se actualiza las variables internas
void PEC1::update_internal(Uint64 delta_time) {
    if (last_effect != NULL) {
        last_effect->update(delta_time);
    }

    if (current_effect != NULL) {
        current_effect->update(delta_time);
    }

    if (current_transition != NULL) {
        current_transition->update(delta_time);
    }
}

void PEC1::required_finished() {
    must_finished = true;
    if (!splash->is_completed()) {
        splash->require_completed();
    } else {
        set_transition(fade_out);
    }
}

bool PEC1::is_finished() {
    return current_effect == NULL ? splash->is_completed() : fade_out->is_completed();
}

void PEC1::set_effect(SDL_Effect* effect) {
    last_effect = current_effect;
    current_effect = effect;
}

void PEC1::set_transition(SDL_Transition* transition) {
    current_transition = transition;
}

Uint64 remain_time = EFFECT_MAX_TIME;

extern std::random_device generator;
extern std::uniform_real_distribution<float> real_distribution;

void PEC1::next_transition() {
    float next_transition = real_distribution(generator);

    if (next_transition < 0.5f) {
        set_transition(circle_trans);
        circle_trans->start();
    } else {
        set_transition(move_trans);
        move_trans->start();
    }
}

// Establecemos un nuevo efecto
void PEC1::next_effect() {
    float next_effect = real_distribution(generator);
    
    if (current_effect == plasma_effect) {
        if (next_effect < 0.5f) {
            set_effect(fire_effect);
            fire_effect->start();
        } else {
            set_effect(wave_effect);
        }
    } else if (current_effect == wave_effect) {
        if (next_effect < 0.5f) {
            set_effect(plasma_effect);
            float next_palette = real_distribution(generator);
            if (next_palette < 0.3f) {
                plasma_effect->set_palette(palette_plasma1);
            } else if (next_palette < 0.6f) {
                plasma_effect->set_palette(palette_plasma2);
            } else {
                plasma_effect->set_palette(palette_plasma3);
            }
        } else {
            set_effect(fire_effect);
            fire_effect->start();
        }
    } else {
        if (next_effect < 0.5f) {
            set_effect(wave_effect);
        } else {
            set_effect(plasma_effect);
            float next_palette = real_distribution(generator);
            if (next_palette < 0.3f) {
                plasma_effect->set_palette(palette_plasma1);
            } else if (next_palette < 0.6f) {
                plasma_effect->set_palette(palette_plasma2);
            } else {
                plasma_effect->set_palette(palette_plasma3);
            }
        }
    }
}

// Actualiza el programa
void PEC1::update(Uint64 delta_time) {
    update_internal(delta_time);

    if (!splash->is_completed()) {
        // Comprobamos si hay que mostrar el splash
        splash->update(delta_time);

        // Si se ha completado, pasamos a fade in si no hay que finalizar el programa
        if (splash->is_completed() && !must_finished) {
            set_effect(plasma_effect);
            set_transition(fade_in);
        }
    } else if (current_transition != NULL && current_transition->is_completed()) {
        // Si no estamos en medio de una transición, comprobamos el tiempo que tenemos entre efectos
        if (remain_time < delta_time) {
            remain_time = EFFECT_MAX_TIME;
            next_transition();
            next_effect();
        } else {
            remain_time -= delta_time;
        }
    }
}

// Se dibuja en pantalla el efecto y la transición actual
void PEC1::draw(SDL_Surface* surface) {
    // Limpiamos la superficie (para que el fade in/out funcione)
    SDL_FillRect(surface, NULL, 0);

    if (!splash->is_completed()) {
        // Se muestra el splash si hace falta
        splash->draw(surface);
    } else {
        // Si se ha completado el splash, mostramos los efectos
        if (last_effect != NULL) {
            last_effect->draw();
        }

        if (current_effect != NULL) {
            current_effect->draw();
        }

        if (current_transition != NULL && !current_transition->is_completed()) {
            current_transition->draw(
                surface,
                last_effect != NULL ? last_effect->get_layer() : NULL,
                current_effect != NULL ? current_effect->get_layer() : NULL
            );
        } else if (current_effect != NULL) {
            if (SDL_BlitSurface(current_effect->get_layer(), NULL, surface, NULL)) {
                std::cerr << "Cannot blit! SDL_Error: " << SDL_GetError() << std::endl;
            }
        }
    }
}
