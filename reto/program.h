#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "config.h"

#ifndef PEC1_PROGRAM_H
#define PEC1_PROGRAM_H

// Representa un efecto a pintar
class SDL_Effect {
    protected:
        SDL_Surface* layer;

    public:
        SDL_Effect();
        ~SDL_Effect();

        // Obtiene la surface interna del efecto
        SDL_Surface* get_layer();
        // Actualiza la ventana
        virtual void update(Uint64 delta_time) = 0;
        // Dibuja en su propia capa
        virtual void draw() = 0;
};

// Representa una transicion entre efectos
class SDL_Transition {
    protected:
        bool completed = false;

    public:
        virtual ~SDL_Transition() = default;

        // Obtiene si la transición está completa
        bool is_completed();
        // Actualiza la ventana
        virtual void update(Uint64 delta_time) = 0;
        // Dibuja en la ventana
        virtual void draw(SDL_Surface* surface, SDL_Surface* last_effect, SDL_Surface* current_effect) = 0;
};

// Estructura que albergará nuestro programa SDL
class SDL_Program {
    public:
        virtual ~SDL_Program() = default;

        // Actualiza la ventana
        virtual void update(Uint64 delta_time) = 0;
        // Dibuja en la ventana
        virtual void draw(SDL_Surface* surface) = 0;
};

#endif // PEC1_Program
