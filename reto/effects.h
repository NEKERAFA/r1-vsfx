#include <cstdint>

#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "config.h"
#include "utils.h"
#include "program.h"

#ifndef PEC1_EFFECTS_H
#define PEC1_EFFECTS_H

class PlasmaEffect : public SDL_Effect {
    private:
        SDL_Color* palette;
        Uint8* buffer1 = NULL;
        Uint8* buffer2 = NULL;
        vector2i buffer1_offset;
        vector2i buffer2_offset;

    public:
        PlasmaEffect();
        ~PlasmaEffect();
        void set_palette(SDL_Color* palette);
        void update(Uint64 delta_time) override;
        void draw() override;
};

class FireEffect : public SDL_Effect {
    private:
        SDL_Color* palette;
        Uint8* last_buffer = NULL;
        Uint8* current_buffer = NULL;

        void add_fire(Uint8* buffer);
        Uint8 convolve(int x, int y);
    
    public:
        FireEffect();
        ~FireEffect();
        void start();
        void set_palette(SDL_Color* palette);
        void update(Uint64 delta_time) override;
        void draw() override;
};

class WaveEffect : public SDL_Effect {
    private:
        SDL_Surface* image;
        int8_t* movement_h = NULL;
        int8_t* movement_v = NULL;
        vector2i movement_h_offset;
        vector2i movement_v_offset;

    public:
        WaveEffect();
        ~WaveEffect();
        void set_image(SDL_Surface* image);
        void update(Uint64 delta_time) override;
        void draw() override;
};

#endif // PEC1_EFFECTS_H