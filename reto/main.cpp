#include <iostream>
#include <random>
#include <functional>
#include <stdlib.h>

// Para unix (Linux/macOS) la ruta al header es SDL2/SDL.h, cosa que en Windows es solo el fichero
#ifdef _WIN32
#include <SDL.h>
#include <SDL_mixer.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#endif

#include "config.h"
#include "program.h"
#include "pec1.h"

// Milisegundos por frame
const int TICKS_PER_FRAME = 1000 / FRAMES_PER_SECOND;

std::random_device generator;
std::uniform_real_distribution<float> real_distribution(0.0f, 1.0f);

Mix_Music* bgm = NULL;

bool init_window(SDL_Window*& window, SDL_Surface*& surface) {
    // Se inicializa el módulo SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "SDL could not initalize! SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }
    
    // Se crea la ventana
    window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        std::cerr << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }

    // Obtiene la superficie de la ventana
    surface = SDL_GetWindowSurface(window);
    return true;
}

void free_window(SDL_Window* window) {
    // Se destruye la ventana
    SDL_DestroyWindow(window);

    // Finalizamos los subsistemas de SDL
    SDL_Quit();
}

bool init_music() {
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
        std::cerr << "SDL_Mixer could not initialize! SDL_Mixer Error: " << Mix_GetError() << std::endl;
        return false;
    }

    bgm = Mix_LoadMUS(BGM_FILE);
    if (bgm == NULL) {
        std::cout << "Warning: Failed to load music! SDL_Mixer Error: " << Mix_GetError() << std::endl;
    } else if (Mix_FadeInMusic(bgm, -1, 2000) < 0) {
        std::cerr << "Cannot play music! SDL_Mixer Error: " << Mix_GetError() << std::endl;
        return false;
    }

    return true;
}

void free_music() {
    if (bgm != NULL) Mix_FreeMusic(bgm);
    Mix_Quit();
}

int main(int argc, char* args[]) {
    // Ventana donde se renderizará el contenido
    SDL_Window* window = NULL;
    // Superficie principal
    SDL_Surface* surface = NULL;

    // Se inicializa la ventana
    bool sdl_initialized = init_window(window, surface);
    if (sdl_initialized) {
        // Inicializamos el programa
        PEC1* program = new PEC1(surface);

        // Inicializamos la música de fondo
        init_music();

        // Inicializamos el bucle principal
        bool quit = false;
        bool quiting = false;
        Uint64 last_time = SDL_GetTicks64(), current_time = 0ul, delta_time = 0ul;
        SDL_Event event;

        while (!quit || !program->is_finished()) {
            // Comprobamos los eventos
            SDL_PollEvent(&event);

            if (!quiting) {
                // Comprobamos si tenemos que finalizar el programa
                if (event.type == SDL_QUIT) {
                    quit = true;
                }

                // Comprobamos si se ha presionado la tecla ESCAPE
                if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) {
                    quit = true;
                }

                if (quit) {
                    quiting = true;
                    program->required_finished();
                    if (bgm != NULL) Mix_FadeOutMusic(2000);
                }
            }

            // Se ejecuta el programa
            if (!quit || !program->is_finished()) {
                // Calculamos el tiempo transcurrido desde el último frame
                current_time = SDL_GetTicks64();
                delta_time = current_time - last_time;

                // Se actualizan los efectos
                program->update(delta_time);

                // Dibujamos en pantalla
                program->draw(surface);

                // Se actualiza el buffer
                SDL_UpdateWindowSurface(window);

                // Esperamos el tiempo entre frames
                if (delta_time < TICKS_PER_FRAME) {
                    SDL_Delay(TICKS_PER_FRAME - delta_time);
                }
                last_time = current_time;
            }
        }

        // Se destruye el programa
        delete program;

        // Se destruye la musica
        free_music();

        // Destruimos la ventana y se finaliza SDL
        free_window(window);
    }

    return sdl_initialized ? EXIT_SUCCESS : EXIT_FAILURE;
}
