#include <iostream>
#include <random>
#include <functional>
#include <cmath>
#include <cstdint>
#include <cstring>

// Para unix (Linux/macOS) la ruta al header es SDL2/SDL.h, cosa que en Windows es solo el fichero
#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "../config.h"
#include "../utils.h"
#include "../program.h"
#include "../effects.h"

// Tamaño de los buffers
const int BUFFER_WIDTH = SCREEN_WIDTH * 2;
const int BUFFER_HEIGHT = SCREEN_HEIGHT * 2;

// Inicializamos los buffers
WaveEffect::WaveEffect(): SDL_Effect(), movement_h(new int8_t[BUFFER_WIDTH * BUFFER_HEIGHT]), movement_v(new int8_t[BUFFER_WIDTH * BUFFER_HEIGHT]) {
    // Establecemos el valor de los buffers
    int pos = 0;
    for (int y = 0; y < BUFFER_HEIGHT; y++) {
        for (int x = 0; x < BUFFER_WIDTH; x++) {
            movement_h[pos] = static_cast<int8_t>(WAVE_LENGTH * std::sin(2.0 * PI * (x + y) / (WAVE_LENGTH * 2)));
            movement_v[pos] = static_cast<int8_t>(WAVE_LENGTH * std::cos(2.0 * PI * (x + y) / (WAVE_LENGTH * 2)));
            pos++;
        }
    }
}

// Liberamos los buffers
WaveEffect::~WaveEffect() {
    delete[] movement_h;
    delete[] movement_v;
}

void WaveEffect::set_image(SDL_Surface* image) { this->image = image; }

void WaveEffect::update(Uint64 delta_time) {
    Uint64 current_time = SDL_GetTicks64();

    // se actualiza cada ventana siguiendo funciones sinusoidales
    movement_h_offset.x = (SCREEN_WIDTH - 1) * (std::cos(2.0 * PI * current_time / (BUFFER_WIDTH * 8)) + 1.0) / 2.0;
    movement_h_offset.y = (SCREEN_HEIGHT - 1) * (std::sin(2.0 * PI * current_time / (BUFFER_HEIGHT * 16)) + 1.0) / 2.0;
    movement_v_offset.x = (SCREEN_WIDTH - 1) * (std::sin(2.0 * PI * current_time / (BUFFER_WIDTH * 16)) + 1.0) / 2.0;
    movement_v_offset.y = (SCREEN_HEIGHT - 1) * (std::cos(2.0 * PI * current_time / (BUFFER_HEIGHT * 8)) + 1.0) / 2.0;
}

void WaveEffect::draw() {
    // Se comprueba que la imagen se haya cargado para poder pintarla
    if (image != NULL) {
        Uint8* image_buffer = (Uint8*) image->pixels;
        int image_bpp = image->format->BytesPerPixel;
        int image_pitch = image->pitch;

        SDL_LockSurface(layer);

        Uint8* dst = (Uint8*) layer->pixels;
        for (int y = 0; y < SCREEN_HEIGHT; y++) {
            for (int x = 0; x < SCREEN_WIDTH; x++) {
                // Obtenemos los valores desplazamiento
                int dx = x + (movement_h[x + movement_h_offset.x + (y + movement_h_offset.y) * BUFFER_WIDTH] >> 2);
                int dy = y + (movement_v[x + movement_v_offset.x + (y + movement_v_offset.y) * BUFFER_WIDTH] >> 2);

                // Obtenemos el valor del pixel
                if ((dx >= 0) && (dx < SCREEN_WIDTH - 1) && (dy >= 0) && (dy < SCREEN_HEIGHT - 1)) {
                    Uint32* src = (Uint32*) ((Uint8*) image_buffer + dy * image_pitch + dx * image_bpp);
                    *((Uint32*) dst) = *src;
                } else {
                    *((Uint32*) dst) = 0;
                }

                dst += layer->format->BytesPerPixel;
            }
        }

        SDL_UnlockSurface(layer);
    }
}