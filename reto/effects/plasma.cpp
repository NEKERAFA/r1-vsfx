#include <cstdint>

#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "../config.h"
#include "../utils.h"
#include "../program.h"
#include "../effects.h"

// Tamaño de los buffers de plasma
const int BUFFER_WIDTH = SCREEN_WIDTH * 2;
const int BUFFER_HEIGHT = SCREEN_HEIGHT * 2;

// Se inicializan los buffers
PlasmaEffect::PlasmaEffect() : SDL_Effect(), buffer1(new Uint8[BUFFER_WIDTH * BUFFER_HEIGHT]), buffer2(new Uint8[BUFFER_WIDTH * BUFFER_HEIGHT]) {
    int pos = 0;
    for (int y = 0; y < BUFFER_HEIGHT; y++) {
        for (int x = 0; x < BUFFER_WIDTH; x++) {
            buffer1[pos] = (Uint8)(64.0 + 63.0 * std::sin(std::hypot(SCREEN_HEIGHT - y, SCREEN_WIDTH - x) / 16.0));
            buffer2[pos] = (Uint8)(64.0 + 63.0 * std::sin(x / (37.0 + 15.0 * std::cos(y / 74.0))) * std::cos(y / (31.0 + 11.0 * std::sin(x / 57.0))));
            pos++;
        }
    }
}

// Se destruyen los buffers
PlasmaEffect::~PlasmaEffect() {
    delete[] buffer1;
    delete[] buffer2;
}

void PlasmaEffect::set_palette(SDL_Color* palette) { this->palette = palette; }

void PlasmaEffect::update(Uint64 _delta_time) {
    Uint64 current_time = SDL_GetTicks64();

    // se actualiza cada ventana siguiendo funciones sinusoidales
    // value = Amplitud * sin(2 * pi * t / frecuencia de oscilación (en ms))
    buffer1_offset.x = (SCREEN_WIDTH - 1) * (std::cos(2.0 * PI * current_time / 8640.0) + 1.0) / 2.0;
    buffer1_offset.y = (SCREEN_HEIGHT - 1) * (std::sin(2.0 * PI * current_time / 9460.0) + 1.0) / 2.0;
    buffer2_offset.x = (SCREEN_WIDTH - 1) * (std::sin(2.0 * PI * current_time / 4530.0) + 1.0) / 2.0;
    buffer2_offset.y = (SCREEN_HEIGHT - 1) * (std::cos(2.0 * PI * current_time / 2590.0) + 1.0) / 2.0;
}

void PlasmaEffect::draw() {
    SDL_LockSurface(layer);

    for (int y = 0; y < SCREEN_HEIGHT; y++) {
        for (int x = 0; x < SCREEN_WIDTH; x++) {
            // Obtenemos los valores de los dos bufferes
            Uint8 value1 = buffer1[x + buffer1_offset.x + (y + buffer1_offset.y) * BUFFER_WIDTH];
            Uint8 value2 = buffer2[x + buffer2_offset.x + (y + buffer2_offset.y) * BUFFER_WIDTH];
            Uint8 value = value1 + value2 % UINT8_MAX;

            // Se calcula el color mediante la paleta actual
            SDL_Color color = palette[value];

            // Pintamos el pixel
            draw_pixel(layer, x, y, color.r, color.g, color.b);
        }
    }

    SDL_UnlockSurface(layer);
}
