#include <random>
#include <functional>
#include <cmath>
#include <cstdint>
#include <cstring>

#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "../config.h"
#include "../utils.h"
#include "../program.h"
#include "../effects.h"

// Matriz de convolución
const int FIRE_WIDTH = 5;
const int FIRE_HEIGHT = 5;
const int FIRE_FILTER[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0};

// Variables para obtener valores aeatorios
extern std::random_device generator;
extern std::uniform_real_distribution<float> real_distribution;
std::uniform_int_distribution<int> screen_distribution(0, SCREEN_WIDTH);

// Se inicializa los buffers
FireEffect::FireEffect(): SDL_Effect(), last_buffer(new Uint8[SCREEN_WIDTH * SCREEN_HEIGHT]), current_buffer(new Uint8[SCREEN_WIDTH * SCREEN_HEIGHT]) {
    add_fire(last_buffer);
}

// Se destruyen los buffers
FireEffect::~FireEffect() {
    delete[] last_buffer;
    delete[] current_buffer;
}

void FireEffect::start() {
    for (int i = 0; i < SCREEN_WIDTH * SCREEN_HEIGHT; i++) {
        last_buffer[i] = 0;
        current_buffer[i] = 0;
    }

    SDL_FillRect(layer, NULL, SDL_MapRGBA(layer->format, 0, 0, 0, 255));
}

// Añade fuego a la última fila del buffer
void FireEffect::add_fire(Uint8* buffer) {
    for (int x = 0; x < 8; x++) {
        buffer[SCREEN_WIDTH * (SCREEN_HEIGHT - 1) + screen_distribution(generator)] = UINT8_MAX * real_distribution(generator);
    }
}

// Aplica el filtro sobre un pixel
Uint8 FireEffect::convolve(int x, int y) {
    int acc = 0;
    int sum = 0;

    for (int j = 0; j < FIRE_HEIGHT; j++) {
        for (int i = 0; i < FIRE_WIDTH; i++) {
            if (FIRE_FILTER[i + j * FIRE_WIDTH] == 0) continue;
            int nx = i - FIRE_WIDTH / 2 + x;
            if (nx < 0 || nx >= SCREEN_WIDTH) continue;
            int ny = j - FIRE_HEIGHT / 2 + y;
            if (ny < 0 || ny >= SCREEN_HEIGHT) continue;

            acc += last_buffer[nx + ny * SCREEN_WIDTH] * FIRE_FILTER[i + j * FIRE_WIDTH];
            sum++;
        }
    }

    return (Uint8)(acc / sum);
}

void FireEffect::set_palette(SDL_Color* palette) { this->palette = palette; }

void FireEffect::update(Uint64 current_time) {
    // Actualizamos el nuevo buffer
    for (int y = 0; y < SCREEN_HEIGHT; y++) {
        for (int x = 0; x < SCREEN_WIDTH; x++) {
            current_buffer[x + y * SCREEN_WIDTH] = convolve(x, y);
        }
    }

    // Añadimos nuevo fuego
    add_fire(current_buffer);
}

void FireEffect::draw() {
    SDL_LockSurface(layer);

    for (int y = 0; y < SCREEN_HEIGHT; y++) {
        for (int x = 0; x < SCREEN_WIDTH; x++) {
            // Obtenemos los valores de los dos bufferes
            Uint8 value = last_buffer[x + y * SCREEN_WIDTH];
            SDL_Color color = palette[value];

            // Pintamos el pixel
            draw_pixel(layer, x, y, color.r, color.g, color.b);
        }
    }

    SDL_UnlockSurface(layer);

    // Actualizamos el buffer
    std::memcpy(last_buffer, current_buffer, sizeof(Uint8) * SCREEN_WIDTH * SCREEN_HEIGHT);
}
