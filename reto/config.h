#include <string>
#include <cstdint>

#ifndef PEC1_CONFIG_H
#define PEC1_CONFIG_H

// Título de la ventana
constexpr char WINDOW_TITLE[] = "PEC 1 - Mi propia demo";

// Ficheros a cargar
constexpr char SPLASH_FILE[] = "splash.png";
constexpr char IMAGE_FILE[] = "background.png";
constexpr char BGM_FILE[] = "caramelldansen.ogg";

// Frames por segundo
const int FRAMES_PER_SECOND = 60;

// Tamaño de la ventana
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

// 2 segundos por transicion
const uint64_t TRANS_MAX_TIME = 2000;

// 10 segundos por efecto
const uint64_t EFFECT_MAX_TIME = 10000;

// Tamaño de la onda
const int WAVE_LENGTH = 8;

#endif // PEC1_CONFIG_H
