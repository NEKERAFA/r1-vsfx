#include <cstdint>

#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "config.h"
#include "utils.h"

float clamp01(float v) { return v > 0 ? (v < 1 ? v : 1) : 0; }
Uint8 lerp(Uint8 a, Uint8 b, float t) { return a + clamp01(t) * (b - a); }

void add_gradient(SDL_Color* colors, Uint8 min, Uint8 max, SDL_Color min_color, SDL_Color max_color) {
    Uint8 length = max - min;
    for (int i = min; i <= max; i++) {
        colors[i].r = lerp(min_color.r, max_color.r, (float)(i - min) / (float)length);
        colors[i].g = lerp(min_color.g, max_color.g, (float)(i - min) / (float)length);
        colors[i].b = lerp(min_color.b, max_color.b, (float)(i - min) / (float)length);
        colors[i].a = UINT8_MAX;
    }
}

void draw_pixel(SDL_Surface* surface, int x, int y, Uint8 r, Uint8 g, Uint8 b) {
    if ((x >= 0) && (x < SCREEN_WIDTH) && (y >= 0) && (y < SCREEN_HEIGHT)) {
        Uint32* pixel = (Uint32*) ((Uint8*) surface->pixels + y * surface->pitch + x * surface->format->BytesPerPixel);
        *pixel = SDL_MapRGBA(surface->format, r, g, b, 255);
    }
}

Uint32* get_pixel(SDL_Surface* surface, int x, int y) {
    return (Uint32*) ((Uint8*) surface->pixels + y * surface->pitch + x * surface->format->BytesPerPixel);
}