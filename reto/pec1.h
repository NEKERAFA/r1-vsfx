#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "config.h"
#include "program.h"
#include "effects.h"
#include "transitions.h"

#ifndef PEC1_H
#define PEC1_H

class Splash {
    private:
        enum Status {FADE_IN, SHOW, FADE_OUT, COMPLETED};

        SDL_Surface* image;
        Uint64 remain_time = TRANS_MAX_TIME;
        Status status = FADE_IN;

    public:
        Splash(SDL_Surface* surface);
        ~Splash();
        void require_completed();
        bool is_completed();
        void update(Uint64 delta_time);
        void draw(SDL_Surface* surface);
};

// Estructura de la clase del Reto 1
class PEC1 : public SDL_Program {
    private:
        bool must_finished = false;

        Splash* splash = NULL;
        SDL_Effect* last_effect = NULL;
        SDL_Effect* current_effect = NULL;
        SDL_Transition* current_transition = NULL;

        void update_internal(Uint64 delta_time);
        void next_transition();
        void next_effect();

    public:
        PEC1(SDL_Surface* surface);
        ~PEC1();

        void required_finished();
        bool is_finished();
        void set_effect(SDL_Effect* effect);
        void set_transition(SDL_Transition* transition);
        void update(Uint64 delta_time) override;
        void draw(SDL_Surface* surface) override;
};

#endif // PEC1_H
